
<?php include_once 'include/header.inc.php'; ?>
<?php include_once 'include/menu.inc.php';?>
 <!--Pequeno Container-->
    <div class="row container">
    <div class="cols s12">
        <h5 class="light">Edição de registros</h5><hr>
    </div>
    </div>
 <!--codigo editar/update-->    
<?php
include_once 'banco_de_dados/conexao.php';

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
$_SESSION['id']=$id;
$querySelect =$link->query("select *  from tb_clientes where id='$id'");

while($registros= $querySelect->fetch_assoc()):
 
    $nome=$registros['nome'];
    $email=$registros['email'];
    $telefone=$registros['telefone'];
endwhile;
?>
 <!--Formulário-->
<div class="row container">
    <p>&nbsp;</p>
    <form class="col s12" action="banco_de_dados/create.php" method="post">

    <fieldset class="formulario" style="padding:15" >
        <legend>Cadastro</legend>
        <h5 class="light center">Alteração</h5>
        
        <?php
            if(isset($_SESSION['msg'])):
                echo $_SESSION['msg'];
                session_unset();
            endif;  
        ?>
        <!--Nome-->
        <div class="row">
        <div class="input-field col s12">
         <i class="material-icons prefix">account_circle</i> 
            <input type="text" name="nome" id="nome" value="<?php echo $nome ?>" maxlength="40" required autofocus>
          <label for="nome">Nome do cliente</label>
        </div>
        </div>
        <!--email-->
        <div class="row">
        <div class="input-field col s12">
        <i class="material-icons prefix">email</i>
          <input type="email" name="email" id="email" value="<?php echo $email ?>" maxlength="50" required>
          <label for="email">Email do cliente</label>
        </div>
        </div>
        <!--Telefone-->
        <div class="row">
        <div class="input-field col s12">
        <i class="material-icons prefix">phone</i>
            <input type="tel" name="telefone"  id="telefone" value="<?php echo $telefone ?>" maxlength="15" required>
            <label for="telefone">Telefone do cliente</label>
        </div>
        </div>

        <div class="imput-field col s12">
            <input type="submit" class="btn blue" value="Alterar">
            <a href="consultas.php" class="btn red">cancelar</a>
            
        </div>    
    </fieldset>  
    </form>
  </div>
<?php
include_once 'include/footer.inc.php';

?>