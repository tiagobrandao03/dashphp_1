<?php session_start(); ?>
<?php include_once 'include/header.inc.php'; ?>
<?php include_once 'include/menu.inc.php';?>

<div class="row container">
    <p>&nbsp;</p>
    <form class="col s12" action="banco_de_dados/create.php" method="post">

    <fieldset class="formulario" style="padding:15" >
        <legend>Cadastro</legend>
        <h5 class="light center">Clientes</h5>
        
        <?php
            if(isset($_SESSION['msg'])):
                echo $_SESSION['msg'];
                session_unset();
            endif;  
        ?>
        <!--Nome-->
        <div class="row">
        <div class="input-field col s12">
         <i class="material-icons prefix">account_circle</i> 
            <input type="text" name="nome" id="nome" maxlength="40" required autofocus>
          <label for="nome">Nome do cliente</label>
        </div>
        </div>
        <!--email-->
        <div class="row">
        <div class="input-field col s12">
        <i class="material-icons prefix">email</i>
          <input type="email" name="email" id="email" maxlength="50" required>
          <label for="email">Email do cliente</label>
        </div>
        </div>
        <!--Telefone-->
        <div class="row">
        <div class="input-field col s12">
        <i class="material-icons prefix">phone</i>
            <input type="tel" name="telefone"  id="telefone"  maxlength="15" required>
            <label for="telefone">Telefone do cliente</label>
        </div>
        </div>

        <div class="imput-field col s12">
            <input type="submit" class="btn blue" value="Cadastrar">
            <input type="submit" class="btn red" value="Limpar">
            
        </div>    
    </fieldset>  
    </form>
  </div>
 
<?php
include_once 'include/footer.inc.php';
?>
 